var body = document.body;
if(localStorage.getItem("theume") == "dark")
{ 
    body.classList.add("theme-dark");
    
} else{
    body.classList.add("theme-light");
}
document.writeln("<script type='text/javascript' src='assets/plugins/jQuery/jquery-3.5.1.min.js'></script>");
document.writeln("<script type='text/javascript' src='assets/plugins/popper/popper.min.js'></script>");
document.writeln("<script type='text/javascript' src='assets/plugins/bootstrap/js/bootstrap.min.js'></script>");

document.writeln("<script type='text/javascript' src='assets/plugins/owl/js/owl.carousel.min.js'></script>");
document.writeln("<script type='text/javascript' src='assets/plugins/slick/js/slick.min.js'></script>");

document.writeln("<script type='text/javascript' src='assets/js/app.js'></script>");
document.writeln("<script type='text/javascript' src='assets/js/app2.js'></script>");