/* aravind */
$(document).ready(function(){
    
    $(".comment-box").focus();
    $(document).scroll(function(){
        var $newpost = $(window).scrollTop();
        if($newpost > 200) {
            $('.btn-newposts').addClass('active');
        } else {
            $('.btn-newposts').removeClass('active');
        }
      });
      /* switcher :: START */
      $(".switcher").on("click", function(){
          console.log("change");
      });
      /* switcher :: END */
      /*template gallery :: START */
      $(".template-btn").on("click", function(){
          $("footer").toggleClass("templates-footer");
      });
      /*template gallery :: END */
      /*new post :: START */
      $(".btn-newposts").on("click", function(){
        $('html, body').animate({scrollTop:0},500);
      });
      /*new post :: END */
      /*landing screen :: START */
     /* var windowHeight = $(window).innerHeight();
      $(".page-widget").css("height", windowHeight);*/
      /*landing screen :: END */
      /* black overlay click 3 dots :: START */
        $(document).on("click", ".ep-followup-list", function(e){
          $(body).addClass("black-overlay");
          $(this).css("z-index", 10);
         if($(this).find("ul").hasClass("follw-active")){
            $(this).find("ul").removeClass("follw-active");
          }
          else{
            $(".ep-followup-list").find("ul").removeClass("follw-active");
            $(this).find("ul").addClass("follw-active");
          } 
      });
    
      $(document).on("click", ".black-overlay", function(e){
          $(".ep-followup-list").css("z-index", 0);
        $(".ep-followup-list").find("ul").removeClass("follw-active");
        $(body).removeClass("black-overlay");
    });
    /* black overlay click 3 dots :: END */
      $(".ep-reaction ul li").on("click", function(e){
          if($(this).hasClass("icon-filled")){
              e.preventDefault();
            var filled_icon = $(this).find("i").attr("data-filled");
            var outline_icon = $(this).find("i").attr("data-outline");
            $(this).find("i").removeClass(outline_icon);
            $(this).find("i").addClass(filled_icon);
          }
       
    });

    $('.market-place-slider').owlCarousel({
        stagePadding: 50,
        loop:true,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
    $('.market-place-details').owlCarousel({
        stagePadding: 0,
        loop:true,
        margin:0,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:5
            }
        }
    });
    $('.related-ads').owlCarousel({
        stagePadding: 40,
        margin: 15,
        loop:false,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
  });
  $('.events-slider').owlCarousel({
    stagePadding: 20,
    margin: 15,
    loop:false,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});

