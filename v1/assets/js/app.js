if(localStorage.getItem("theume") == "dark"){
    $("#dark-theume").attr("checked", "checked");
}else{
    $("#dark-theume").removeAttr("checked");
}
$(function () {
    
    // Ripple Effect :: START
    $(".material-ripple").mousedown(function (e) {
        var ink = new Ink($(this), e);
    });

    function Ink(parent, e) {

        this.el = $("<span>", { class: "material-ink" });

        parent.prepend(this.el);

        d = Math.max(parent.outerWidth(), parent.outerHeight());
        this.el.css({ height: d, width: d });

        x = e.pageX - parent.offset().left - d / 2;
        y = e.pageY - parent.offset().top - d / 2;

        this.el
            .css({ top: y + "px", left: x + "px" })
            .addClass("material-ink-animate");

        this.destroy(500);

    }

    Ink.prototype.destroy = function (time) {

        var that = this; // Gotta love this (pun intended)

        setTimeout(function () {
            that.el.remove();
            that = undefined;
        }, time);
    };
    // Ripple Effect :: END
    $(".form-control").focus(function(){
        $(this).parent('.form-group').addClass("form-focused");
    });
    $(".form-control").focusout(function(){
        if($(this).val() === ""){
            $(this).parent('.form-group').removeClass("form-focused");
        }       
    });
    $('.clear-dataicon i').on('click', function(){
        var empVal = $(this).parent('.clear-dataicon').parent('.form-group').find(".form-control");
        empVal.val('');
        empVal.removeClass("form-focused");
    });
    $('.owl-newpost').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots:true,
        autoplay: true,
        responsive:{
            0:{
                items:1
            }
        }
    });
    $('.owl-toptrends').owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:4,
                stagePadding: 17
            }
        }
    });
    $('.owl-suggested').owlCarousel({
        loop:true,
        margin:5,
        nav:false,
        dots:false,
        center:false,
        stagePdding:25,
        responsive:{
            0:{
                items: 2,
        stagePadding: 40,
        margin: 5,
            }
        }
    });
    // File Upload
    $(".upload").on('change', function() {
        var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
        $(this).next(".uploaded-file").remove();
        if(filename != ""){
            $(this).parents(".file-upload").append('<p class="uploaded-file">'+filename+'</p>');
        }
    });
    $(".form-control-material").focus(function(){
        $(this).parent().addClass("form-control-material-focused");
    });
    $(".form-control-material").focusout(function(){
        if($(this).val() === "")
        $(this).parent().removeClass("form-control-material-focused");
    });
    $(".dark-theume").change(function(){
       
    });
    // $('.dark-theume-lbl').click(function() {
    //     if($("#dark-theume").attr('checked', false)){
    //         localStorage.setItem("theume", "light");
    //     }
    // });
    // $('.dark-theume-lbl').click(function() {
    //     if ($("#dark-theume").attr('checked', true)) {
    //         localStorage.setItem("theume", "dark");
    //     }
    // });
    $('#dark-theume').change(function() {
        if(localStorage.getItem("theume") == "dark"){
            localStorage.setItem("theume", "light");
            $("body").addClass("theme-light");
            $("body").removeClass("theme-dark");
            
        }else{
            localStorage.setItem("theume", "dark");
            $("body").addClass("theme-dark");
            $("body").removeClass("theme-light");
        }
    });
});

